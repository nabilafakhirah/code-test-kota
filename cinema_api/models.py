from django.db import models

# Create your models here.
class Cinema(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_no = models.CharField(max_length=12)

class Studio(models.Model):
    room_no = models.IntegerField(default=1)
    cinema = models.CharField(max_length=50)

class Seats(models.Model):
    cinema = models.CharField(max_length=50)
    studio = models.IntegerField(default=1)
    seat_no = models.CharField(max_length=3) # example: A03

class Schedule(models.Model):
    cinema = models.CharField(max_length=50)
    studio = models.IntegerField(default=1)
    movie = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()

class Tickets(models.Model):
    cinema = models.CharField(max_length=50)
    studio = models.IntegerField(default=1)
    movie = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    seat_no = models.CharField(max_length=3)

class Movie(models.Model):
    name = models.CharField(max_length=100)
    director = models.CharField(max_length=30)