from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.views.decorators.csrf import csrf_exempt
from .serializers import *

class CinemaCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = CinemaSerializer(data=data)

        if serializer.is_valid():
            cinema = serializer.save()
            if cinema:
                return Response(serializer.data, status= status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class CinemaUpdate(APIView):
    @staticmethod
    @csrf_exempt
    def put(request):
        data = request.data
        saved_cinema = Cinema.objects.get(name=data['name'])
        serializer = CinemaSerializer(instance=saved_cinema, data=data, partial=True)
        if serializer.is_valid():
            cinema = serializer.save()
            if cinema:
                return Response(serializer.data, status = status.HTTP_202_ACCEPTED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class CinemaView(APIView):
    @staticmethod
    def get(request, cinema_name):
        cinema = Cinema.objects.get(name=cinema_name)
        serializer = CinemaSerializer(cinema, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllCinema(APIView):
    @staticmethod
    def get(request):
        cinema = Cinema.objects.all()
        serializer = CinemaSerializer(cinema, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class StudioCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = StudioSerializer(data=data)

        if serializer.is_valid():
            studio = serializer.save()
            if studio:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class StudioView(APIView):
    @staticmethod
    def get(request, cinema):
        studio = Studio.objects.filter(cinema=cinema)
        serializer = StudioSerializer(studio, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllStudio(APIView):
    @staticmethod
    def get(request):
        studio = Studio.objects.all()
        serializer = StudioSerializer(studio, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class SeatsCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = SeatsSerializer(data=data)

        if serializer.is_valid():
            seats = serializer.save()
            if seats:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class SeatsView(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        seats = Seats.objects.filter(cinema= data['cinema'], studio=data['studio'])
        serializer = SeatsSerializer(seats, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllSeats(APIView):
    @staticmethod
    def get(request):
        seats = Seats.objects.all()
        serializer = SeatsSerializer(seats, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class ScheduleCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = ScheduleSerializer(data=data)

        if serializer.is_valid():
            schedule = serializer.save()
            if schedule:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class ScheduleUpdate(APIView):
    @staticmethod
    @csrf_exempt
    def put(request):
        data = request.data
        saved_schedule = Schedule.objects.get(pk=data['id'])
        serializer = ScheduleSerializer(instance=saved_schedule, data=data, partial=True)
        if serializer.is_valid():
            schedule = serializer.save()
            if schedule:
                return Response(serializer.data, status = status.HTTP_202_ACCEPTED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class ScheduleView(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        movie = request.data['movie']
        schedule = Schedule.objects.filter(movie=movie)
        serializer = ScheduleSerializer(schedule, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllSchedule(APIView):
    @staticmethod
    def get(request):
        schedule = Schedule.objects.all()
        serializer = ScheduleSerializer(schedule, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class TicketsCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = TicketsSerializer(data=data)

        if serializer.is_valid():
            tickets = serializer.save()
            if tickets:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class TicketsUpdate(APIView):
    @staticmethod
    @csrf_exempt
    def put(request):
        data = request.data
        saved_tickets = Cinema.objects.get(pk=data['id'])
        serializer = TicketsSerializer(instance=saved_tickets, data=data, partial=True)
        if serializer.is_valid():
            tickets = serializer.save()
            if tickets:
                return Response(serializer.data, status = status.HTTP_202_ACCEPTED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class TicketsView(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        movie = request.data['movie']
        tickets = Tickets.objects.filter(movie=movie)
        serializer = TicketsSerializer(tickets, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllTickets(APIView):
    @staticmethod
    def get(request):
        tickets = Tickets.objects.all()
        serializer = TicketsSerializer(tickets, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class MovieCreate(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        data = request.data
        serializer = MovieSerializer(data=data)

        if serializer.is_valid():
            movie = serializer.save()
            if movie:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class MovieView(APIView):
    @staticmethod
    @csrf_exempt
    def post(request):
        name = request.data['name']
        movie = Movie.objects.filter(name=name)
        serializer = MovieSerializer(movie, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AllMovies(APIView):
    @staticmethod
    def get(request):
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)