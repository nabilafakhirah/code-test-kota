from rest_framework import serializers

from cinema_api.models import Cinema, Studio, Seats, Schedule, Tickets, Movie

class CinemaSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50, required=True)
    address = serializers.CharField(max_length=200, required=True)
    phone_no = serializers.CharField(max_length=12, required=True)

    def create(self, validated_data: Cinema):
        cinema = Cinema.objects.create(name = validated_data['name'],
                                       address = validated_data['address'],
                                       phone_no = validated_data['phone_no'])
        return cinema

    def update(self, instance, validated_data):
        instance.address = validated_data['address']
        instance.phone_no = validated_data['phone_no']

        instance.save()
        return instance

    class Meta:
        model = Cinema
        fields = ('name', 'address', 'phone_no')

class StudioSerializer(serializers.ModelSerializer):
    room_no = serializers.IntegerField(required=True)
    cinema = serializers.CharField(max_length=50, required=True)

    def create(self, validated_data: Studio):
        studio = Studio.objects.create(room_no = validated_data['room_no'],
                                       cinema = validated_data['cinema'])
        return studio

    class Meta:
        model = Studio
        fields = ('room_no', 'cinema')

class SeatsSerializer(serializers.ModelSerializer):
    cinema = serializers.CharField(max_length=50, required=True)
    studio = serializers.IntegerField(required=True)
    seat_no = serializers.CharField(required=True, max_length=3)

    def create(self, validated_data: Seats):
        seats = Seats.objects.create(cinema = validated_data['cinema'],
                                     studio = validated_data['studio'],
                                     seat_no = validated_data['seat_no'])
        return seats

    class Meta:
        model = Seats
        fields = ('cinema', 'studio', 'seat_no')

class ScheduleSerializer(serializers.ModelSerializer):
    cinema = serializers.CharField(max_length=50, required=True)
    studio = serializers.IntegerField(required=True)
    movie = serializers.CharField(required=True, max_length=100)
    date = serializers.DateField(required=True)
    time = serializers.TimeField(required=True)

    def create(self, validated_data: Schedule):
        schedule = Schedule.objects.create(cinema = validated_data['cinema'],
                                           studio = validated_data['studio'],
                                           movie = validated_data['movie'],
                                           date = validated_data['date'],
                                           time = validated_data['time'])
        return schedule

    def update(self, instance, validated_data):
        instance.cinema = validated_data['cinema']
        instance.studio = validated_data['studio']
        instance.movie = validated_data['movie']
        instance.date = validated_data['date']
        instance.time = validated_data['time']

        instance.save()
        return instance

    class Meta:
        model = Schedule
        fields = ('cinema', 'studio', 'movie', 'date', 'time')

class TicketsSerializer(serializers.ModelSerializer):
    cinema = serializers.CharField(max_length=50, required=True)
    studio = serializers.IntegerField(required=True)
    movie = serializers.CharField(required=True, max_length=100)
    date = serializers.DateField(required=True)
    time = serializers.TimeField(required=True)
    seat_no = serializers.CharField(required=True, max_length=3)

    def create(self, validated_data: Tickets):
        tickets = Tickets.objects.create(cinema = validated_data['cinema'],
                                         studio = validated_data['studio'],
                                         movie = validated_data['movie'],
                                         date = validated_data['date'],
                                         time = validated_data['time'],
                                         seat_no = validated_data['seat_no'])
        return tickets

    def update(self, instance, validated_data):
        instance.cinema = validated_data['cinema']
        instance.studio = validated_data['studio']
        instance.movie = validated_data['movie']
        instance.date = validated_data['date']
        instance.time = validated_data['time']
        instance.seat_no = validated_data['seat_no']

        instance.save()
        return instance

    class Meta:
        model = Tickets
        fields = ('cinema', 'studio', 'movie', 'date', 'time', 'seat_no')

class MovieSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, max_length=100)
    director = serializers.CharField(required=True, max_length=30)

    def create(self, validated_data: Movie):
        movie = Movie.objects.create(name = validated_data['name'],
                                     director = validated_data['director'])
        return movie

    class Meta:
        model = Movie
        fields = ('name', 'director')