from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Cinema)
admin.site.register(Studio)
admin.site.register(Seats)
admin.site.register(Schedule)
admin.site.register(Tickets)
admin.site.register(Movie)