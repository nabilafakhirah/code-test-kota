from django.conf.urls import url
from . import views
app_name = 'cinema_api'

urlpatterns = [
    url(r'^create_cinema/$', views.CinemaCreate.as_view(), name='create_cinema'),
    url(r'^update_cinema/$', views.CinemaUpdate.as_view(), name='update_cinema'),
    url(r'^cinema_view/(?P<cinema_name>\w+)', views.CinemaView.as_view(), name='view_cinema'),
    url(r'^all_cinema/$', views.AllCinema.as_view(), name='all_cinema'),
    url(r'^create_studio/$', views.StudioCreate.as_view(), name='create_studio'),
    url(r'^studio_view/(?P<cinema>\w+)', views.StudioView.as_view(), name='view_studio'),
    url(r'^all_studio/$', views.AllStudio.as_view(), name='all_studio'),
    url(r'^create_seats/$', views.SeatsCreate.as_view(), name='create_sets'),
    url(r'^seats_view/$', views.SeatsView.as_view(), name='view_seats'),
    url(r'^all_seats/$', views.AllSeats.as_view(), name='all_seats'),
    url(r'^create_schedule/$', views.ScheduleCreate.as_view(), name='create_schedule'),
    url(r'^update_schedule/$', views.ScheduleUpdate.as_view(), name='update_schedule'),
    url(r'^schedule_view/$', views.ScheduleView.as_view(), name='view_schedule'),
    url(r'^all_schedule/$', views.AllSchedule.as_view(), name='all_schedule'),
    url(r'^create_tickets/$', views.TicketsCreate.as_view(), name='create_tickets'),
    url(r'^update_tickets/$', views.TicketsUpdate.as_view(), name='update_tickets'),
    url(r'^tickets_view/$', views.TicketsView.as_view(), name='view_tickets'),
    url(r'^all_tickets/$', views.AllTickets.as_view(), name='all_tickets'),
    url(r'^create_movie/$', views.MovieCreate.as_view(), name='create_movie'),
    url(r'^movie_view/$', views.MovieView.as_view(), name='view_movie'),
    url(r'^all_movie/$', views.AllMovies.as_view(), name='all_movies'),
]
